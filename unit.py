import Adafruit_BBIO.GPIO as GPIO
import serial
import time

class Unit:
    def __init__(self, outlet_serial, outlet, triger_in, triger_out, ir_out,
            rs232, outlet_cmd_on, outlet_cmd_off):
        self.outlet_serial = outlet_serial
        self.outlet = outlet
        self.unit_gpio_in = triger_in
        self.unit_gpio_out = triger_out
        self.unit_gpio_ir = ir_out
        self.outlet_code_on = outlet_cmd_on
        self.outlet_code_off = outlet_cmd_off
        self.unit_serial = rs232

        self.time_off = 10
        self.time_on = 10
        self.cycles = 1000
        self.timer = 0
        self.control = 0
        self.unit_code_on = 'null'
        self.unit_code_off = 'null'
        self.state = False
        self.error = False
        self.sequence = 0


    def set_unit_state(self, state):
        self.state = state
        if self.control == 0: # RS232
            if self.state:
                self.unit_serial.write(str.encode(self.unit_code_on))
            else:
                self.unit_serial.write(str.encode(self.unit_code_off))
        elif self.control == 1: # Power Control
            if self.state: GPIO.output(self.unit_gpio_out, GPIO.HIGH)
            else: GPIO.output(self.unit_gpio_out, GPIO.LOW)
        else: # IR
            if self.state: self.ir_driver(unit_code_on)
            else: self.ir_driver(unit_code_off)
        time.sleep(1)
        self.outlet_state(state)



    def check_errors(self):
        if self.control == 0: # RS232
            self.error = False
        elif self.control == 1: # Power Control
            self.error = False
        else: # IR
            self.error = False


    def outlet_state(self, state):
        if state:
            print(str.encode(self.outlet_code_on))
            self.outlet_serial.write(str.encode(self.outlet_code_on))
            print("Turned oulet " + str(self.outlet) + " on")
        else:
            self.outlet_serial.write(str.encode(self.outlet_code_off))
            print("Turned outlet " + str(self.outlet) + " off")


    def ir_driver(self, code):
        print(code)


    def cycle(self, time):
        if self.state:
            self.check_errors()
        else: self.error = False
        self.set_unit_state(not self.state)
        if self.state: self.timer = time + self.time_on
        else: self.timer = time + self.time_off

