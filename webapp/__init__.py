import os
import json
import sys
import socket
import time

from threading import Thread
from flask import Flask, render_template, request, redirect, url_for
from cycle import Cycle

from tcpcom import TCPServer

IP_PORT = 22000

def onStateChanged(state, msg):
    if state == "LISTENING":
        print("Listening")
    elif state == "CONNECTED":
        print("Connected")
    elif state == "MESSAGE":
        print("Message")

def create_app(test_config=None):
    app = Flask(__name__)

    server = TCPServer(IP_PORT, stateChanged = onStateChanged)

    @app.route('/hello')
    def hello():
        return 'Hello, World!'

    @app.route('/setup', methods=['GET', 'POST'])
    def setup():
        if 'start' in request.form:
            settings ={
                    "0":{
                        "cycles": request.form['cycle_1'],
                        "time_on": request.form['on_1'],
                        "time_off": request.form['off_1'],
                        "cmd_on": request.form['cmd_on_1'],
                        "cmd_off": request.form['cmd_off_1'],
                        "mode": request.form['mode_1']
                        },
                    "1":{
                        "cycles": request.form['cycle_2'],
                        "time_on": request.form['on_2'],
                        "time_off": request.form['off_2'],
                        "cmd_on": request.form['cmd_on_2'],
                        "cmd_off": request.form['cmd_off_2'],
                        "mode": request.form['mode_2']
                        },
                    "2":{
                        "cycles": request.form['cycle_3'],
                        "time_on": request.form['on_3'],
                        "time_off": request.form['off_3'],
                        "cmd_on": request.form['cmd_on_3'],
                        "cmd_off": request.form['cmd_off_3'],
                        "mode": request.form['mode_3']
                        },
                    "3":{
                        "cycles": request.form['cycle_4'],
                        "time_on": request.form['on_4'],
                        "time_off": request.form['off_4'],
                        "cmd_on": request.form['cmd_on_4'],
                        "cmd_off": request.form['cmd_off_4'],
                        "mode": request.form['mode_4']
                        }
                    }
            settings = json.dumps(settings)
            print(str(settings))
            server.sendMessage(str(settings))
            return redirect(url_for('running'))
        return render_template('setup.html')

    @app.route('/running', methods=['GET', 'POST'])
    def running():
        if 'play' in request.form:
            server.sendMessage("start")
        elif 'stop' in request.form:
            setver.sendMessage("stop")
        return render_template('running.html')

    return app
