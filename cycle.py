from flask import Flask, request
from threading import Thread
from unit import Unit
import Adafruit_BBIO.GPIO as GPIO
import Adafruit_BBIO.UART as UART
import serial
import time
import sys
import socket
import json
from tcpcom import TCPClient

IP_ADDRESS = "127.0.0.1"
IP_PORT = 22000


pc_inputs = ["P8_8", "P9_30", "P9_25", "P9_15"]
pc_outputs = ["P9_41", "P9_27", "P9_23", "P9_12"]
ir_outputs = ["P8_9", "P8_11", "P8_7"]

def onStateChanged(state, msg):
    global cycle
    if state == "CONNECTING":
        print("Connecting")
    elif state == "CONNECTED":
        print("Connected")
    elif state == "DISCONNECTED":
        print("Disconnected")
    elif state == "MESSAGE":
        print("Message: " + str(msg))
        if msg == "start":
            cycle.play_pause()
        elif msg == "stop":
            cycle.stop()
        elif msg == "pause":
            cycle.pause()
        else:
            print("test")
            cycle.setup(str(msg))


def main():
    global cycle
    cycle = Cycle()
    client = TCPClient(IP_ADDRESS, IP_PORT, stateChanged = onStateChanged)
    rc = client.connect()
    print("Runtime Started")
    while True:
        cycle.runtime()


class Cycle:
    def __init__(self):
        #GPIO.setup("P9_12", GPIO.OUT) #Trig4 Out
        #GPIO.setup("P9_15", GPIO.IN)  #Trig4 In
        #GPIO.setup("P9_27", GPIO.OUT) #Trig2 Out
        #GPIO.setup("P9_30", GPIO.IN)  #Trig2 In
        #GPIO.setup("P9_25", GPIO.IN)  #Trig3 In
        #GPIO.setup("P9_23", GPIO.OUT) #Trig3 Out
        #GPIO.setup("P8_8", GPIO.IN)  #Trig1 In
        #GPIO.setup("P9_41", GPIO.OUT) #Trig1 Out
        #GPIO.setup("P8_7", GPIO.OUT) #IR3 Out
        #GPIO.setup("P8_9", GPIO.OUT) #IR1 Out
        #GPIO.setup("P8_11", GPIO.OUT) #IR2 Out

        for pin in pc_inputs:
            GPIO.setup(pin, GPIO.IN)
            GPIO.add_event_detect(pin, GPIO.RISING)

        for pin in pc_outputs:
            GPIO.setup(pin, GPIO.OUT)

        for pin in ir_outputs:
            GPIO.setup(pin, GPIO.OUT)

        UART.setup("UART1")
        UART.setup("UART2")
        UART.setup("UART4")
        UART.setup("UART5")

        self.ser_1 = serial.Serial(port = "/dev/ttyO1", baudrate=115200)
        self.ser_2 = serial.Serial(port = "/dev/ttyO2", baudrate=9600)
        self.ser_3 = serial.Serial(port = "/dev/ttyO4", baudrate=9600)
        self.ser_4 = serial.Serial(port = "/dev/ttyO5", baudrate=9600)

        self.events = [False, False, False, False]

        self.triger_in = ["P8_8", "P9_30", "P9_25", "P9_15"]
        self.triger_out = ["P9_41", "P9_27", "P9_23", "P9_12"]
        self.ir_out = ["P8_9", "P8_11", "P8_7" "null"]
        self.rs232 = [self.ser_2, self.ser_3, self.ser_4, "null"]
        self.outlet_cmd_on = ["(IOX 3 4 1)", "(IOX 3 5 1)", "(IOX 3 6 1)",
                "(IOX 3 7 1)"]
        self.outlet_cmd_off = ["(IOX 3 4 0)", "(IOX 3 5 0)", "(IOX 3 6 0)",
                "(IOX 3 7 0)"]

        self.units = []
        for i in range(0,3):
            temp_unit = Unit(self.ser_1, i, self.triger_in[i],
                        self.triger_out[i], self.ir_out[i], self.rs232[i],
                        self.outlet_cmd_on[i], self.outlet_cmd_off[i])
            self.units.append(temp_unit)

        self.run = True
        self.paused = True
        self.time = time.time()

        self.ser_1.write(str.encode("(PWR 1)"))
        time.sleep(1)
        self.ser_1.write(str.encode("(TST 1 132)"))



    def setup(self, settings):
        print("setup start")
        json_settings = json.loads(settings)
        for i in range(0,4):
            self.units[i].cycles = json_settings[i]["cycles"]
            self.units[i].time_on = json_settings[i]["time_on"]
            self.units[i].time_off = json_settings[i]["time_off"]
            self.units[i].unit_code_on = json_settings[i]["cmd_on"]
            self.units[i].unit_code_off = json_settings[i]["cmd_off"]
            self.units[i].control = json_settings[i]["mode"]
        print("Cycle Setup")


    def start(self):
        self.paused = False
        print("Cycle Started")


    def stop(self):
        self.paused = True
        for unit in self.units:
            unit.cycles = 0
        print("Cycle Stoped")


    def pause(self):
        self.paused = True
        print("Cycle Paused")


    def resume(self):
        self.paused = False
        print("Cycle Resumed")


    def play_pause(self):
        self.paused = not self.paused
        if self.paused:
            print("Cycle Paused")
        else:
            print("Cycle Resumed")


    def update_clock(self):
        self.time = time.time()


    def runtime(self):
        if self.run:
            if not self.paused:
                for unit in self.units:
                    if unit.cycles > 0:
                        current_time = time.time()
                        if current_time >= unit.timer:
                            print("Action")
                            unit.cycle(current_time)


if __name__ == "__main__":
    main()
